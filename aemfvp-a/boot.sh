#!/bin/bash

# Copyright (c) 2021-2024, ARM Limited and Contributors. All rights reserved.
# Copyright (c) 2024, Linaro Limited. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

aemfvp_a_valid_arguments=("uefi" "u-boot" "distro")
aemfvp_a_rme_valid_arguments=("distro" "shell" "validate" "kvm-unit-tests" "realm-distro-boot" "realm-guest" "ssh-connection" "tftf")

__print_examples()
{
	echo "Example 1: ./boot.sh -p aemfvp-a -b [uefi|u-boot]"
	echo "  Starts the execution of the aemfvp-a model and the software boots up to the"
	echo "  busybox prompt. The selection of the bootloader to be either uefi or u-boot"
	echo "  is based on command line parameters."
	echo ""
	echo "Example 2: ./boot.sh -p [aemfvp-a-rme|aemfvp-a] -b distro -d <absolute_path_to_distro>"
	echo "  Starts the execution of the aemfvp-a-rme model and boots the specified distro"
	echo "  were successfully booted."
	echo ""
	echo "Example 3: ./boot.sh -p aemfvp-a-rme -b shell"
	echo "  Starts the execution of the aemfvp-a-rme model and the software boots up to the"
	echo "  buildroot prompt."
	echo ""
	echo "Example 4: ./boot.sh -p aemfvp-a-rme -b validate"
	echo "  Starts the execution of the aemfvp-a-rme model to validate that four worlds"
	echo "  were successfully booted."
	echo ""
	echo "Example 5: ./boot.sh -p aemfvp-a-rme -b kvm-unit-tests"
	echo "  Starts the execution of the aemfvp-a-rme model to run the kvm unit tests"
	echo "  were successfully booted."
	echo ""
	echo "Example 6: ./boot.sh -p aemfvp-a-rme -b realm-distro-boot"
	echo "  Starts the execution of the aemfvp-a-rme model to run the realm distro boot test"
	echo "  were successfully booted."
	echo ""
	echo "Example 7: ./boot.sh -p aemfvp-a-rme -b realm-guest"
	echo "  Starts the execution of the aemfvp-a-rme model to run the realm guest test"
	echo "  were successfully booted."
	echo ""
	echo "Example 8: ./boot.sh -p aemfvp-a-rme -b ssh-connection"
	echo "  Starts the execution of the aemfvp-a-rme model to run the ssh connection test"
	echo "  were successfully booted."
	echo ""
	echo "Example 9: ./boot.sh -p aemfvp-a-rme -b tftf"
	echo "  Starts the execution of the aemfvp-a-rme model to run the tftf test"
	echo "  were successfully booted."
	echo ""
}

print_usage ()
{
	echo ""
	echo "Execute a specified test case on the aemfvp-a platform."
	echo "Usage: ./boot.sh -p <platform> -b <test_case> [-d <distro>]"
	echo ""
	echo "Supported command line parameters - "
	echo "  -p   Specifies the platform to be selected"
	echo "		Supported platform is -"
	echo "	     	aemfvp-a aemfvp-a-rme"
	echo "  -b   Specifies which boot target or test to run"
	echo "		Supported bootloader options are for aemfvp-a -"
	echo "		${aemfvp_a_valid_arguments[@]}"
	echo "		Supported bootloader options are for aemfvp-a-rme -"
	echo "		${aemfvp_a_valid_arguments[@]}"
	echo "  -d   Specifies the location of the distro image"
	echo"        (this parameter is optional and only required when booting distros"
	echo ""
	__print_examples
	echo ""
}

# Scan for free local ports, starting at specified value
function get_free_port {
        local port=$1
        local used=$(netstat -lat | tail -n+3 | awk '{print $4}' | cut -d ':' -f2)
        while : ; do
                if ( ! echo ${used} | grep -q ${port} ) then
                        echo ${port}
                        return
                fi
                port=$(expr ${port} + 1)
                [[ ${port} -lt 65536 ]] || port=1
                [[ ${port} != ${1} ]] || return
        done
}

4_world_boot_test () 
{
	echo "Waiting for the system to start"
	until [ -f $log_dir/$uart0_log ]; do sleep 0.1; done
	grep -q "Welcome to Buildroot" <( tail -f $log_dir/$uart0_log )

	sleep 1

	local secure_status=1
	local rmm_status=1
	local nonsecure_status=1

	# Parse secure world string
	grep -q "INFO: Hafnium initialisation completed" $log_dir/$uart0_log
	secure_status=$?
	[ $secure_status == 0 ] \
		&& echo "[INFO]: Secure world boot successful" \
		|| echo "[ERROR]: Secure world boot unsuccessful"

	# Parse realm world string
	grep -q "INFO.*RMM init end" $log_dir/$uart0_log
	rmm_status=$?
	[ $rmm_status == 0 ] \
		&& echo "[INFO]: Realm world boot successful" \
		|| echo "[ERROR]: Realm world boot unsuccessful"

	# Parse non-secure world string
	grep -q "Welcome to Buildroot" $log_dir/$uart0_log
	nonsecure_status=$?
	[ $nonsecure_status == 0 ] \
		&& echo "[INFO]: Non-Secure world boot successful" \
		|| echo "[ERROR]: Non-Secure world boot unsuccessful"

	# Error if any of the previous fails
	[[ $secure_status == 0 && $rmm_status == 0 && $nonsecure_status == 0 ]] \
		&& echo "[INFO]: Four world boot successful"; return 0 \
		|| echo "[ERROR]: Four world boot unsuccessful"; return 1
}

run_fvp()
{
        export container_id=$platform-fvp-$BOOT_MODE-$(uuidgen)
	export log_dir="$(pwd)/logs/$container_id/$platform"
	mkdir -p $log_dir
	volumes=" $volumes --volume $out_dir:$out_dir --volume $log_dir:/var/log/fvp/ --volume $model_scripts_dir/aemfvp-a/:/tmp/aemfvp-a/"

	j2 $YAML.new.j2 > $YAML
	image=$(cat $YAML | shyaml get-value boot.docker.name)
	path=$(cat $YAML | shyaml get-value boot.image)
	arguments=$(cat $YAML | shyaml get-value boot.arguments | sed 's/^..//')

	echo "running docker run --rm --hostname aemfvp-a --name $container_id $volumes $image $path $arguments"
	docker run --rm --hostname aemfvp-a --name $container_id --privileged $volumes $image $path $arguments | tee $log_dir/model.log &
        sleep 1	

	until [ -f $log_dir/model.log ]; do sleep 0.1; done

	if ! command -v xterm &> /dev/null
	then
		echo "xterm not found please install if required outputting uart0 to terminal"
		export $no_xterm=true
		exit 1
	fi

	while [[  -z  $UART_0_PORT ]] 
	do
		UART_0_PORT=$(cat $log_dir/model.log | grep terminal_0 | cut -d' ' -f8)
		UART_1_PORT=$(cat $log_dir/model.log | grep terminal_1 | cut -d' ' -f8)
		UART_2_PORT=$(cat $log_dir/model.log | grep terminal_2 | cut -d' ' -f8)
		UART_3_PORT=$(cat $log_dir/model.log | grep terminal_3 | cut -d' ' -f8)
	done

	if [ "$Automate" = true ]; then
		echo "started fvp in a container use docker exec -it $container_id telnet 127.0.0.1 <PORT> to connect, where ports are as folows:"
		echo "uart0:$UART_0_PORT"
		echo "uart1:$UART_1_PORT"
		echo "uart2:$UART_2_PORT"
		echo "uart3:$UART_3_PORT"
	        docker exec -t $container_id telnet 127.0.0.1 $UART_0_PORT > /dev/null 2>&1 &
	        docker exec -t $container_id telnet 127.0.0.1 $UART_1_PORT > /dev/null 2>&1 &
	        docker exec -t $container_id telnet 127.0.0.1 $UART_2_PORT > /dev/null 2>&1 &
	        docker exec -t $container_id telnet 127.0.0.1 $UART_3_PORT > /dev/null 2>&1 &
	else
		echo "starting xterms for $container_id"
		xterm -xrm 'XTerm.vt100.allowTitleOps: false' -T $container_id-uart0 -e bash -c "docker exec -it $container_id telnet 127.0.0.1 $UART_0_PORT" &
		xterm -xrm 'XTerm.vt100.allowTitleOps: false' -T $container_id-uart1 -e bash -c "docker exec -it $container_id telnet 127.0.0.1 $UART_1_PORT" &
		xterm -xrm 'XTerm.vt100.allowTitleOps: false' -T $container_id-uart2 -e bash -c "docker exec -it $container_id telnet 127.0.0.1 $UART_2_PORT" &
		xterm -xrm 'XTerm.vt100.allowTitleOps: false' -T $container_id-uart3 -e bash -c "docker exec -it $container_id telnet 127.0.0.1 $UART_3_PORT" &
	fi

}
setup_fvp()
{

	# Choose correct fvp exec path.
	HOST_ARCH="$(uname -m)"
	export FVP_MODEL_PATH="Linux64_GCC-9.3"
	if [ "$HOST_ARCH" == "aarch64" ]; then
		export FVP_MODEL_PATH="Linux64_armv8l_GCC-9.3"
	fi

	case $BOOT_MODE in
	#aemfvp-a
	   "u-boot")
	       echo "BOOTING THE NON-RME STACK WITH U-BOOT"
	       export FIP="$out_dir/$platform/fip-uboot.bin"
	       export ROOTFS="$out_dir/components/$platform/grub-busybox.img"
	       run_fvp
	       ;;

	    "uefi")
	       echo "BOOTING THE NON-RME STACK WITH UEFI"
	       export ROOTFS="$out_dir/components/$platform/grub-busybox.img"
	       run_fvp
	       ;;

	#common
	    "distro")
	       echo "BOOTING THE DISTRO"
	       export ROOTFS_BOOT_ARG=$ROOTFS_BOOT_ARG_DISTRO
   	       export ROOTFS=$DISTRO
	       volumes="$volumes --volume $DISTRO:$DISTRO"
	       run_fvp
	       ;;

	#aemfvp-a-rme
	    "shell")
	       echo "BOOTING THE RME STACK TO SHELL"
	       export FIP="$out_dir/fip.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	        ;;

	    "validate")
	       echo "VALIDATE FOUR WORLD BOOT"
	       export FIP="$out_dir/fip.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	       4_world_boot_test
               docker stop $container_id 
	       [ $? -eq 0 ] && exit 0 || exit 1
	       ;;

	    "kvm-unit-tests")
	       echo "RUNNING KVM UNIT TESTS"
	       export FIP="$out_dir/fip.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	       docker exec -it $container_id /tmp/aemfvp-a/testcases/kvm-unit-tests.sh >> $log_dir/kvm-unit-tests.log  2>&1
	       docker stop $container_id
	       ;;

	    "realm-distro-boot")
	       echo "RUNNING REALM DISTRO BOOT"
	       export FIP="$out_dir/fip.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	       docker exec -it $container_id /tmp/aemfvp-a/testcases/realm_distro_boot.sh >> $log_dir/realm_distro_boot.log  2>&1
	       docker stop $container_id
	       ;;

	    "realm-guest")
	       echo "RUNNING REALM GUEST BOOT"
	       export FIP="$out_dir/fip.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	       docker exec -it $container_id /tmp/aemfvp-a/testcases/realm_guest.sh >> $log_dir/realm_guest.log  2>&1
	       docker stop $container_id
	       ;;

	    "ssh-connection")
	       echo "RUNNING SSH CONNECTION"
	       export FIP="$out_dir/fip.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	       docker exec -it $container_id /tmp/aemfvp-a/testcases/ssh_connection.sh >> $log_dir/ssh_connection.log  2>&1
	       docker stop $container_id
	       ;;

	    "tftf")
	       echo "RUNNING TFTF TEST"
	       export FIP="$out_dir/fip-std-tests.bin"
	       export BL1="$out_dir/bl1.bin"
	       export ROOTFS="$out_dir/host-fs.ext4"
	       run_fvp
	       docker exec -it $container_id /tmp/aemfvp-a/testcases/tftf.sh >> $log_dir/tftf.log  2>&1
	       echo "Test ended"
	       docker stop $container_id
	       ;;

	     *)
		echo ""
		echo "[ERROR]: unsupported or missing boot_mode"
		print_usage
		exit 1
		;;
	esac

}

check_boot_mode() {
	args=("$@")
	for i in "${args[@]}"
	do 
		if [[ "$BOOT_MODE" == "$i" ]]; then
			valid_arg="true"
		fi
	done
}

export FVP_DOCKER_IMAGE="fvp:aemva-11.27_19"
export START_MESSAGE="NA"
export START_TIMEOUT="NA"
export LOGIN_INFORMATION="NA"
export PROMPT="NA"
export ROOTFS_BOOT_ARG="bp.virtioblockdevice.image_path"
AARCH64_VERSION="v9-5"

while getopts "p:b:h:d:a:" opt; do
	case $opt in
		p)
			platform=$OPTARG
			;;
		b)
			BOOT_MODE=$OPTARG
			;;
		d)
			DISTRO=$OPTARG
			;;
		a)
			AARCH64_VERSION=$OPTARG
			;;
		*)
			echo "option not recognised"
			print_usage
			exit 1
			;;
	esac
done

export AARCH64_VERSION=$AARCH64_VERSION

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
WORKSPACE_DIR=$(dirname $(dirname $SCRIPT_DIR))
out_dir="$WORKSPACE_DIR/output/$platform"
model_scripts_dir="$WORKSPACE_DIR/model-scripts"
uart0_log=uart0.log

YAML=$WORKSPACE_DIR//model-scripts/aemfvp-a/templates/boot-$platform.yaml
cp $YAML.j2 $YAML.new.j2
sed -i 's/- boot/boot/g' $YAML.new.j2
sed -i 's/{BL1}/{{ BL1 }}/g' $YAML.new.j2
sed -i 's/{FIP}/{{ FIP }}/g' $YAML.new.j2
sed -i 's/{ROOTFS}/{{ ROOTFS }}/g' $YAML.new.j2
sed -i 's/{ARTIFACTS}/{{ ARTIFACTS }}/g' $YAML.new.j2
sed -i 's/{KERNEL}/{{ KERNEL }}/g' $YAML.new.j2

case $platform in
    "aemfvp-a")
	check_boot_mode "${aemfvp_a_valid_arguments[@]}"

	export BL1="$out_dir/$platform/tf-bl1.bin"
	export ARTIFACTS="$out_dir/$platform/"
	export FIP="$out_dir/$platform/fip-uefi.bin"
	export ROOTFS_BOOT_ARG_DISTRO="pci.pcie_rc.ahci0.ahci.image_path"
	;;

    "aemfvp-a-rme")
	check_boot_mode "${aemfvp_a_rme_valid_arguments[@]}"

	export BL1="$out_dir/bl1.bin"
	export ARTIFACTS="$out_dir"
	export FIP="$out_dir/fip.bin"
	export KERNEL="$out_dir/Image"
	export ROOTFS_BOOT_ARG_DISTRO=$ROOTFS_BOOT_ARG
	export SSH_PORT=$(get_free_port 8022)
	echo "ssh port $SSH_PORT"
	echo "SSH_PORT=$SSH_PORT" > $model_scripts_dir/aemfvp-a/config.sh
	echo "LOG_DIR=/var/log/fvp" >> $model_scripts_dir/aemfvp-a/config.sh
	echo "uart0_log=$uart0_log" >> $model_scripts_dir/aemfvp-a/config.sh
	;;

    *)
	echo "[ERROR] Could not deduce which platform to execute on."
	print_usage
	exit 1
	;;
esac
if [[ "$valid_arg" != "true" ]]; then
	echo "[ERROR] Invalid boot mode selection"
	print_usage
	exit 1
fi
setup_fvp
