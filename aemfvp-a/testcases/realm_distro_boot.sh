#!/bin/bash

# Copyright (c) 2024, ARM Limited and Contributors. All rights reserved.
# Copyright (c) 2024, Linaro Limited. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

source /tmp/aemfvp-a/config.sh
source /tmp/aemfvp-a/fvp-run.sh

#Wait to boot to Complete
until [ -f $LOG_DIR/$uart0_log ]; do sleep 0.1; done
grep -q "Welcome to Buildroot" <( tail -f $LOG_DIR/$uart0_log)

echo "RUNNING UEFI REALM BOOT TEST"

lkvm_uefi_boot_cmd="lkvm run \
                        --realm \
                        --restricted_mem \
                        -c 4 \
                        -m 2500 \
                        --disable-sve \
                        --irqchip=gicv3-its \
                        --force-pci \
                        --console=serial \
                        --measurement-algo=sha256 \
                        --loglevel info \
                        --disk /realm_uefi/debian-12-nocloud-arm64.raw \
                        --firmware /realm_uefi/KVMTOOL_EFI.fd"

if [ "$Automate" = true ]; then
        # In case of automation, store logs in terminal_0.
        fvprun ${lkvm_uefi_boot_cmd} | tee $LOG_DIR/terminal0.log &

        # Wait for the debian boot to present login screen.
        grep -q "Debian GNU/Linux 12 localhost ttyS0" <( tail -f $LOG_DIR/terminal0.log )
else
        fvprun ${lkvm_uefi_boot_cmd}
fi

echo "Test Complete"
